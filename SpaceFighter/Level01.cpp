

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager* pResourceManager)//comment 2
{
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	//sets count
	const int COUNT = 21;
	//sets positions depending on the count
	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	//sets delays depending on the count
	double delays[COUNT] =//spawn rate change
	{
		0.0, 0.3, 0.2,
		1.0, 0.25, 0.25,
		1.25, 0.25, 0.25, 0.25, 0.25,
		1.25, 0.25, 0.25, 0.25, 0.25,
		2.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 2.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)//spawning enemies
	{
		//takes the start delay for enemy spawing in and adds the delay for the others when they spawn in to create a time period
		delay += delays[i];
		//sets the postions on the enemimes while finding the borders of the screen to not put them out of bounds, also load teh texture for the enemies
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);
		//creates a new enemy
		BioEnemyShip* pEnemy = new BioEnemyShip();
		//sets texture for new enemy
		pEnemy->SetTexture(pTexture);
		//sets what level the ememy spawn on, difficulty
		pEnemy->SetCurrentLevel(this);
		//sets the postions it should spawn on
		pEnemy->Initialize(position, (float)delay);
		//adds it to the game
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

